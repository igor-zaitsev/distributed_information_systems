package com.zaitsev.lab7.repos;

import com.zaitsev.lab7.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface User repo.
 */
@Repository
public interface UserRepo extends JpaRepository<User, Long> {
}
