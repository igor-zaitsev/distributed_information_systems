package com.zaitsev.lab7.controller;

import com.zaitsev.lab7.domain.User;
import com.zaitsev.lab7.repos.UserRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * The type Main controller.
 */
@Controller
@RequestMapping("/main")
public class MainController {

    private final UserRepo userRepo;

    /**
     * Instantiates a new Main controller.
     *
     * @param userRepo the user repo
     */
    @Autowired
    public MainController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    /**
     * Gets all users.
     *
     * @param model the model
     * @return the all users
     */
    @GetMapping
    public String getAllUsers(Model model) {
        model.addAttribute("users", userRepo.findAll());
        return "main";
    }

    /**
     * Add user string.
     *
     * @param user the user
     * @return the string
     */
    @PostMapping
    public String addUser(@ModelAttribute User user) {
        userRepo.save(user);
        return "redirect:/main";
    }

    /**
     * Gets one user.
     *
     * @param user  the user
     * @param model the model
     * @return the one user
     */
    @GetMapping("/{id}")
    public String getOneUser(@PathVariable("id") User user,
                             Model model) {
        model.addAttribute("user", user);
        return "editUser";
    }

    /**
     * Edit user string.
     *
     * @param userFormDB the user form db
     * @param user       the user
     * @return the string
     */
    @PostMapping("/{id}")
    public String editUser(@PathVariable("id") User userFormDB,
                           @ModelAttribute User user) {
        BeanUtils.copyProperties(user, userFormDB, "id");
        userRepo.save(userFormDB);
        return "redirect:/main";
    }

    /**
     * Delete user string.
     *
     * @param id the id
     * @return the string
     */
    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        userRepo.deleteById(id);
        return "redirect:/main";
    }
}
