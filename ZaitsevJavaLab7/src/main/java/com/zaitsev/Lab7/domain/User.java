package com.zaitsev.lab7.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * The type User.
 */
@Entity
@Data
@Table(name = "usr")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private Integer age;
}
